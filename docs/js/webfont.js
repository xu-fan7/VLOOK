/**************************************
 *
 * webfont path for VLOOK.js - Typora Plugin
 * (配合 vlook.js 进行使用)
 *
 * V14.0
 * 2022-03-13
 * powered by MAX°孟兆
 *
 * QQ Group: 805502564
 * email: maxchow@qq.com
 *
 * https://github.com/MadMaxChow/VLOOK
 * https://gitee.com/madmaxchow/VLOOK
 *
 *************************************/

 var webfontHost = "https://madmaxchow.github.io/openfonts/";
