---
title: Introduction - VLOOK™ - Theme Package and Enhancement Plug-In for Typora Markdown Editor
author: MAX°孟兆
keywords:
- Markdown, typora, vlook, plugin, plug-in, theme pack, automatic typesetting, cross platform, I18N, open source, MIT, open source China, OSC, editorial recommendation
- Table enhancement, cell merging, row grouping, crosshairs, repeating header, scratch card, black screen, label, picture enhancement, presentation assistance, spotlight, laser pen, auto folding, printing, mermaid, audio, video, phonetic notation, theme, font, template, dark mode, dark mode, cover, back cover, private customization
- PRD, design, requirements, documents, blogs, manuals, guides, online, operation and maintenance, knowledge base, wiki
- Product manager, programmer, operation and maintenance engineer, pre-sales and after-sales
vlook-chp-autonum: h1{{#ALPHA#. }},h2{{STEP-##-min#: }}
vlook-query: effects=2&ws=auto&lmc=1
vlook-doc-lib: vlook-lib-en.html
---

###### ~VLOOK™~<br>Let Your Markdown Have A New Perspective^[Interaction]^<br>──<br><u>Introduction</u><br>`#Latest|V20.0#`<br><br><br>**MAX°孟兆**<br>*COPYRIGHT © 2016-2023. MAX°DESIGN.*

[TOC]

> **选择语言 ❯ **[<kbd>🇨🇳 简体中文</kbd>](index.md)

# What is VLOOK™

`#Editor|Typora#`~(Gy)~ `#OS|macOS#`~(Bk)~ `#OS|Windows#`~(Bu)~ `#License|MIT#`~(Rd)~



>![VLOOK™](https://madmaxchow.gitee.io/vlookres/pic/vlook-mark-light.svg?darksrc=vlook-mark-dark.svg#logo)
>
>**A DOMESTICALLY DEVELOPED OPEN-SOURCE PRODUCT RECOMMENDED BY** ![OSChina](https://madmaxchow.gitee.io/vlookres/pic/oschina.png#icon) **[Open Source China](https://www.oschina.net/p/vlook)**.
>
>![VLOOK](https://madmaxchow.gitee.io/vlookres/pic/vlook-light.svg?darksrc=vlook-dark.svg#icon) [VLOOK](https://github.com/MadMaxChow/VLOOK)™ is a **THEME PACK** and **ENHANCEMENT PLUGIN** for Typora[^Typora], a cross-platform Markdown editor that targets exported HTML files.
>
>VLOOK™ is an open-source software that follows the **MIT License**.
>
>~(T1)~

> Code hosting: [![Github](https://madmaxchow.gitee.io/vlookres/pic/github-light.svg?darksrc=github-dark.svg#logo)](https://github.com/MadMaxChow/VLOOK?lnkcss=none)　　[![Gitee](https://madmaxchow.gitee.io/vlookres/pic/gitee-light.svg?darksrc=gitee-dark.svg#logo)](https://gitee.com/madmaxchow/VLOOK?lnkcss=none)

> > ###### Agreement
>
> VLOOK™ is open source software and complies with the following open source agreements:
>
> *==License Agreement of VLOOK™==*
>
> ```
> MIT License
> Copyright (c) 2016-2021 MAX°DESIGN | Max Chow
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
> ```

---

> > ###### ![Markdown](pic/markdown-mark-solid.svg?fill=text#icon) What is Markdown?
>
> - In 2004, [John Gruber](https://en.wikipedia.org/wiki/John_Gruber) created [![Markdown](pic/markdown-mark-solid.svg?fill=text#icon) Markdown](https://zh.wikipedia.org/wiki/Markdown) , a type of writing specifically for the Internet `Text markup language` . With Markdown, you only need to insert a small number of markup symbols in the writing process, and you can easily typeset (for example, set the title, bold, list, quote, etc.);
> - Markdown documents are based on `Plain text format storage` , which means that they can be opened with almost any text editor. At the same time, it can be exported to rich text documents with typesetting, HTML web pages, etc. through the Markdown editor.Pure, concise, easy to use and flexible, Are the reasons why people like Markdown;
> - The current standardization project of Markdown is [CommonMark](http://commonmark.org) .
>
> ![Markdown](pic/markdown-mark.svg?fill=text#logo)

> > ###### 60 seconds to learn, 10 minutes to learn Markdown grammar in depth
>
> 1. `#RECOMMANDED#`  **Flavored the Markdown GitHub** (GFM) Syntax Reference: Typora the currently used standard [in detail](https://support.typora.io/Markdown-Reference/) ;
> 2. Standardized **CommonMark** grammar reference: [60 seconds to learn Markdown grammar](http://commonmark.org/help/) , [10 minutes to learn Markdown in depth](http://commonmark.org/help/tutorial/) .

[^Typora]: Typora is a cross-platform Markdown editor (perhaps the best editor at the moment), which supports direct preview and editing. For more detailed features, please refer to the [official websit](https://www.typora.io).

# Prepared For

**If you also have one or more of the following needs or pain points, you can safely try the Markdown-based document solution for document editing, publishing, and management. The recommended combination is Typora + VLOOK™:**

- Use Markdown to write documents, but there are more requirements for the Markdown editor, or the typesetting and interaction of the HTML output
- For the written document, hope  `/ ??? |Unified template and output/`~(T1)~ , and best `/ ??? |Change theme any time/`~(T2)~
- I hope I only pay attention to the writing of the content of the document, and the tedious work of typesetting hopes to be ableautomationFinish
- RduceIn terms of software tools such as documents (such as Word) and graphics (such as Visio)Purchase expenditure, Or these software typesettingOperation feels bored
- Need supportCross-platform,Cross-terminalHow to browse and publish documents
- The output documents can provide interactive auxiliary tools (such as catalog outline, spotlight, laser pointer, footnotes, etc.) when reading, reviewing, and demonstrating

---

> 💡 **Do you know?**
>
> This document is created by Typora and using the VLOOK™ plug-in! **AMAZING!!!**
>
> ~(Bn!)~

# Donate

**If you like VLOOK™, you can contribute a cup of coffee :-)**

`#PayPal|@madmaxchow#`~(Cy!)~

[![打赏 VLOOK™](https://madmaxchow.gitee.io/vlookres/pic/donate-paypal-light.png?darksrc=donate-paypal-dark.png&srcset=@2x&darksrcset=@2x#frame)](https://paypal.me/madmaxchow?lnkcss=none)

# Quick Start

VLOOK™ continues to **explore and expand** Markdown and CSS, and at the same time combines the Internet-based application scenarios of documents~

In **the document layout** , **content navigation** , **presentation aid** , **interactive experience** provided and other aspects of the **consistent** , **concise** , **and friendly** experience.

---

>　　　　###### ![°文档排版](pic/qico-types-light.svg?fill=text&darksrc=invert#icon) ° LAYOUT
>
>---
>
>**With the support of VLOOK™ theme and plug-ins, you have a new understanding and application of the automatic typesetting ability of the Markdown editor (only supports Typora for now).**
>
>In addition to rich typography enhancements, based on the HTML format, static documents can also "move" with you~
>
>
>
>[<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide.md#快速入坑°文档排版)
>
>~(Vn!)~

> ###### ![°内容导航](pic/qico-nav-light.svg?fill=text&darksrc=invert#icon) ° NAVIGATION
>
> ---
>
> **VLOOK ™ provides tools for document chapters, figures, tables, and multimedia to provide multiple forms of navigation, rapid positioning, and content organization to improve and improve the browsing experience and efficiency of published HTML files.**
>
> No extra action, focus on document writing. Automatic integration is completed when you click export~
>
> 
>
> [<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide.md#快速入坑°内容导航)
>
> ~(Bu!)~

---

>
>###### ![°演示与出版辅助](pic/qico-pres-light.svg?fill=text&darksrc=invert#icon) ° PRESENTATION & PUBLICATION
>
>---
>
>**VLOOK™ uniquely provides powerful presentation and publishing aids, making Typora + VLOOK a more productive Markdown document solution.**
>
>Very suitable for on-site and remote presentation aids, turn on Turbo mode for your documents~
>
>
>
>[<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide.md#快速入坑°演示与出版辅助)
>
>~(Og!)~

>
>###### ![°主题与不杂项](pic/qico-theme-light.svg?fill=text&darksrc=invert#icon) ° THEME & not MISC.
>
>---
>
>" **Humans have always been visual animals. It is a kind of virtue, a kind of power, and a kind of belief to let the eyes feel pleasure and beauty when reading by themselves or others.** "
>
>　　　　—— MAX°孟兆
>
>
>
>[<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide.md#快速入坑°主题与不杂项)
>
>~(Lm!)~

---

###### **[Click here to browse more VLOOK™ topics ![](pic/icon-forward.svg?fill=text#icon)](vlook-doc-lib://)**

# How To Use

> You can start a brand **new Markdown experience** in **just 3 steps**.
>
> Make your Markdown a new perspective^[Interaction]^!
>

## Download and Configure

---

---

> > ###### Download the Plug-in
>
> 1. Download the latest version from the homepage of VLOOK™ on **[GitHub](https://github.com/MadMaxChow/VLOOK/releases)** or **[Gitee](https://gitee.com/madmaxchow/VLOOK/releases)** ;
>2. You can also [download the theme file](https://github.com/MadMaxChow/VLOOK/tree/master/released/theme) ( [alternate link](https://gitee.com/madmaxchow/VLOOK/tree/master/released/theme) ) directly on the project homepage .
> 
>~(Gn)~

> > ###### Install Typora
>
> 1. Download and install the latest version of [Typora](https://www.typora.io) ;
>2. Start Typora and enter "**Preferences**", enable all options under "**Markdown Extended Syntax, Code Blocks**". See the figure below for details:
> 
>~(Gn)~

> > ###### Install Fonts (optional)
>
> Download and install the VLOOK™ theme matching font pack•• See "[Font Themes](guide.md#字体主题)" for details.
>
> ~(Gn)~

*==Typora ▸ 偏好设置 ▸ Markdown==*

![Typora ▸ Preferences ▸ Markdown](https://madmaxchow.gitee.io/vlookres/pic/typora-opt1-light.png?srcset=@2x&darksrc=typora-opt1-dark.png&darksrcset=@2x)

## Install the Theme Package

---

---

> > ###### Install the Theme
>
> 1. The `released\theme`all CSS files are copied to Typora theme directory;
>2. Where is the theme directory? You can navigate to this directory by clicking "**Preferences ▸ Appearance ▸ Open Theme Directory**".
> 
>~(Og)~

> > ###### Select Theme
>
> 1. Restart Typora;
>2. Click the "**Theme**" menu, select to `Vlook *` any topic can be in the form of naming.
> 
>~(Og)~

> > ###### Start Writing from the Template
>
> It is recommended to create your own Markdown document based on the document template of the VLOOK™ specification, so that you can get started faster.
>
> ~(Og)~

> > ###### Where are the template files?
>
> 1. `*.md` files in the directory `released`
>    1. Standard template: *VLOOK-Document-Template.md*
>    2. No cover template: *VLOOK-Document-Template-nocover.md*
>    3. Document library template: *VLOOK-Document-Template-doc_lib.md*
> 2. You can also download the document templates directly from the [project homepage](https://github.com/MadMaxChow/VLOOK/tree/master/released/demo) ( [alternative link](https://gitee.com/madmaxchow/VLOOK/tree/master/released/demo) )

## Configure Export Options

---

---

---

> > ###### Configure export setting
>
> 1. Start Typora and enter "**Preferences**";
>2. Click "**Export**", add configuration (==select HTML template==), and name the configuration `VLOOK` .
> 
>~(Cy)~

> > ###### Install meta tag code
>
> 1. Open the meta tag file: released\plugin\\[**meta.txt**](https://raw.githubusercontent.com/MadMaxChow/VLOOK/master/released/plugin/meta.txt) ( [alternate link](https://gitee.com/madmaxchow/VLOOK/raw/master/released/plugin/meta.txt) );
>2. Select and copy all contents;
> 3. Paste to "**Preferences ▸ Export ▸ VLOOK ▸ Append in &lt;head /&gt;**".
>
> ~(Cy)~

> > ###### Install the plugin code
>
> 1. Open the plug-in file: released\plugin\\[**plugin.txt**](https://raw.githubusercontent.com/MadMaxChow/VLOOK/master/released/plugin/plugin.txt) ( [alternate link](https://gitee.com/madmaxchow/VLOOK/raw/master/released/plugin/plugin.txt) );
>2. Select and copy all contents;
> 3. Paste to "**Preferences ▸ Export ▸ VLOOK ▸ Append in &lt;body /&gt;**".
>
> ~(Cy)~

> > ###### One-click export
>
> 1. Open the md file conforming to the VLOOK™ specification;
>2. Click "**File ▸ Export ▸ VLOOK** or **VLOOK (live)**".
> 
>~(Cy)~

*==Typora ▸ 偏好设置 ▸ 导出==*

![Typora ▸ Preferences ▸ Export](https://madmaxchow.gitee.io/vlookres/pic/typora-opt2-light.png?srcset=@2x&darksrc=typora-opt2-dark.png&darksrcset=@2x)

# Supplement

> ###### Online version plug-in
>
> ---
>
> The above configuration method is an offline plug-in. The complete plug-in code is integrated with HTML files. It is suitable for situations where an intranet or no network is required when browsing documents, but it is not convenient to update plug-ins in real time and [switch template themes online](guide.md#模板主题) .
>
> At present, VLOOK supports the online version of plug-in. You can refer to the above method to add a new "export configuration" (it is recommended to name the configuration `VLOOK (live)`).
>
> The online version of the plugin file is: released\plugin \ **plugin_live.txt** , or directly open the online version of [plugin_live.txt](https://raw.githubusercontent.com/MadMaxChow/VLOOK/master/released/plugin/plugin_live.txt) ( [alternative link](https://gitee.com/madmaxchow/VLOOK/raw/master/released/plugin/plugin.txt) )
>
> ~(Og!)~

---

> ###### How to update to latest version ?
>
> ---
>
> To upgrade the old version of vlook to the latest version, repeat **step 1** above to download the latest release version, and press **steps 2 and 3** to update the corresponding "theme" and "export configuration".
>
> ~(Cy!)~

> ###### Recommend compatible browser !
>
> ---
>
> In order to ensure the best user experience, it is strongly recommended to use the following browsers to access:
>
> ![Chrome](https://madmaxchow.gitee.io/vlookres/pic/chrome.png#icon2x) **[Chrome](https://www.google.cn/chrome/)**　　![Edge](https://madmaxchow.gitee.io/vlookres/pic/edge.png#icon2x) **[Edge](https://www.microsoft.com/zh-cn/edge)**　　![Firefox](https://madmaxchow.gitee.io/vlookres/pic/firefox.png#icon2x) **[Firefox](https://www.mozilla.org/zh-CN/firefox/)**
>
> ~(Bu)~

---

If you have any suggestions and needs, please feel free to give feedback~ [![VLOOK™ @ Email](https://madmaxchow.gitee.io/vlookres/pic/feedback-light.svg?darksrc=invert#logo)](mailto:67870144@qq.com?subject=Feedback%20about%20VLOOK%20&lnkcss=none)

# The End