---
title: More Topics about VLOOK™
---



---

> ###### ℹ️ INTRODUCTION
>
> ---
>
> What is VLOOK™? For whom? **6 sets** of built-in **themes**, **60+** enhanced **features**...
>
> There is always something that can impress you~
>
> [<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](index-en.md)
>
> ~(T1)~

> ###### 🕹 HOW TO USE
>
> ---
>
> You can start a brand **new Markdown experience** in just **3 steps**.
>
> Make your Markdown a new perspective^[Interaction]^!
>
> [<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](index-en.md#how-to-use)
>
> ~(T2)~



> ###### <center>🎯 GUIDE</center>
>
> ---
>
> > ###### ![](pic/qico-types-light.svg?fill=text#icon) °Layout
> >
> > The automatic typesetting ability of markdown has been comprehensively improved
> >
> > [<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide.md#快速入坑°文档排版)
> >
> > ~(Vn!)~
>
> >
> > ###### ![](pic/qico-nav-light.svg?fill=text#icon) °Navigation
> > 
> > ---
> >
> > Integrated tools for navigation, rapid positioning, and content organization
> >
> > [<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide2.md#快速入坑°内容导航)
> >
> > ~(Bu!)~
>
> ---
>
> > ###### ![](pic/qico-pres-light.svg?fill=text#icon) °Present.＆ Pub.
> >
> > Built in flexible and convenient tools for presentation and publishing
> > [<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide2.md#快速入坑°演示与出版辅助)
> > 
> >~(Og!)~
> 
>> ###### ![](pic/qico-theme-light.svg?fill=text#icon) °Theme＆ not Misc.
> >
> > The exquisite and delicate reading experience is pleasant and beautiful
> >
> > [<kbd>Learn more ![](pic/icon-forward.svg?fill=text#icon)</kbd>](guide2.md#快速入坑°主题与不杂项)
> >
> > ~(Lm!)~
> 
>~(Gy)~



> ###### 📊 SCRIPTED CHARTS
>
> Mermaid is a library for drawing flowcharts, state charts, sequence charts, and Gantt charts. It uses JS for local rendering and is widely integrated in the Markdown editor.
>
> Using Typora + VLOOK™ can provide a better style and experience for Mermaid~
>
> ------
>
> **At present, the latest version of Typora + VLOOK™ can support the creation and generation of scripted charts:**
>
> 
>
> [![流程图](https://madmaxchow.gitee.io/vlookres/pic/dg-flowcharts.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#流程图)[![顺序图](https://madmaxchow.gitee.io/vlookres/pic/dg-seq.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#顺序图)[![状态机图](https://madmaxchow.gitee.io/vlookres/pic/dg-state.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#状态机图)[![类图](https://madmaxchow.gitee.io/vlookres/pic/dg-class.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#类图)[![实体关系图](https://madmaxchow.gitee.io/vlookres/pic/dg-er.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#实体关系图)[![用户旅程地图](https://madmaxchow.gitee.io/vlookres/pic/dg-uj.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#用户旅程地图)[![甘特图](https://madmaxchow.gitee.io/vlookres/pic/dg-gantt.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#甘特图)[![饼图](https://madmaxchow.gitee.io/vlookres/pic/dg-pie.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#饼图)[![Gitgraph 图](https://madmaxchow.gitee.io/vlookres/pic/dg-gitgraph.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#Gitgraph 图)[![思维导图](https://madmaxchow.gitee.io/vlookres/pic/dg-mindmap.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#思维导图)[![需求图](https://madmaxchow.gitee.io/vlookres/pic/dg-req.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#需求图)[![时间线](https://madmaxchow.gitee.io/vlookres/pic/dg-timeline.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#时间线)[![C4 图](https://madmaxchow.gitee.io/vlookres/pic/dg-c4.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#C4 图)[![ZenUML 图](https://madmaxchow.gitee.io/vlookres/pic/dg-zenuml.png?srcset=@2x&darksrc=invert#frame#inline)](chart.md#ZenUML 图)
>
> ~(Ye)~
